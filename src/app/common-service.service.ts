import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  private url = "localhost:1337/"
  constructor(private http:HttpClient) {


   }
    getData(){
      
      return this.http.get(this.url + "abouts").pipe(
        map((res:any) =>{ 
          return res;
        })
      )

    }
}
