import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServicesComponent } from './services/services.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CareerComponent } from './career/career.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { CommonServiceService } from './common-service.service';

@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    HomeComponent,
    AboutUsComponent,
    PortfolioComponent,
    CareerComponent,
    ContactComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {
        path: "home",
        component: HomeComponent,
      },
      {
        path: "about",
        component: AboutUsComponent,
      },
      {
        path: "service",
        component: ServicesComponent,
      },
      {
        path: "portfolio",
        component: PortfolioComponent,
      },
      {
        path: "contact",
        component: ContactComponent,
      },
      {
        path: "career",
        component: CareerComponent,
      },
    ])
  ],
  providers: [CommonServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
