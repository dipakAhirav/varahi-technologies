import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../common-service.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor( private CommonServiceService:CommonServiceService) { }

  ngOnInit(): void {

    this.CommonServiceService.getData().subscribe(data => {
      console.log("this is data:",data)
    })
  }

}
